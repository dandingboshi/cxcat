 **小程序官网:** https://www.cxcat.com

 **小程序地址** https://www.wpstorm.cn

 **联系微信:** hackdxd

**安装教程** 
1. 作者官网安装教程[点击查看](https://cxcat.com/183.html)
2. 零到一安装教程[点击查看](https://blog.csdn.net/qq_37552048/category_11120874.html)

WordPress多端开源小程序,这是丸子团队在发布丸子社区小程序之后，首次打造一款免费简洁的开源小程序。
要维持丸子社区小程序的升级，又要开发一款支持微信和 QQ的小程序前端，真的挺不容易的。
但是，我们还是坚持了下来，希望能够帮助到想要基于 WordPress 程序创建小程序的用户。
这次的开源小程序功能不多，界面清新，简洁。

**开源小程序功能：**
- 文章资源实时同步
- 支持视频在线播放
- 支持海报生成
- 支持分享朋友圈
- 支持搜索关键词文章
- 支持文章点赞 / 收藏 / 评论
- 支持查看个人点赞 / 收藏 / 评论文章列

没有太复杂的功能，除了简洁还是简洁，除了清新还是清新，重要的是，如此优雅的小程序，加载性能好，适用性强。

 **小程序演示截图:** 

![wsVn00.png](https://s1.ax1x.com/2020/09/14/wsVn00.png)
![wsVtn1.png](https://s1.ax1x.com/2020/09/14/wsVtn1.png)

我们已经把开源小程序发布到了WordPress小程序主题资源平台上，如果你对小程序有兴趣。
可以访问 WordPress小程序主题资源平台下载:
https://www.wpstorm.cn

我们已经把这款开源小程序发布到了开源平台上，如果你对这款小程序有兴趣，可以上以下两个平台下载:
地址;
如何下载开源小程序？

复制以上地址在你的浏览器打开，选择下载即可,了解详情（ 记得给我们的开源小程序 Star 哦 ^_^）。
GitHub地址:https://github.com/dchijack/Travel-Mini-Program

GiTee地址：https://gitee.com/izol/Travel-Mini-Program
![](https://upload-images.jianshu.io/upload_images/7606447-269295085632758c.png?imageMogr2/auto-orient/strip|imageView2/2/w/1116/format/webp)
丸子WodPress小程序付费版本(丸子社区/丸子资讯)
强大的后台功能以及简化的小程序设置，几乎不需要更改小程序源码，只要在网站后台简单的配置信息，就能拥有一个漂亮又功能强大的社区小程序。

### 在线体验扫码：


![wseywt.jpg](https://s1.ax1x.com/2020/09/14/wseywt.jpg)

**基本功能**
- 网站小程序数据实时同步更新；
- 首页推荐文章展示；
- 文章/动态/点赞/评论/收藏/转发；
- 文章海报生成；
- 评论跟随；
- 强大的广告功能；
- 支持文章内容转换语音朗诵，
- 采用百度语音/讯飞语音技术，支持用户自行选择平台；

**社区功能**

- 小程序端发表社区话题，发布文章;
- 小程序文章链接分享，可自动获取分享链接的内容，支持的分享链接包括:微信公众号、抖音、微博、B站文章内容;
- 小程序视频链接分享，可自动获取分享视频地址，支持的分享视频包括：微博视频、抖音、VueVideo及特定的腾讯视频链接；
- 支持用户关注，可以互相关注，随时可以了解你喜欢的人发布的内容;
- 积分功能：支持阅读、评论、发布及签到积分，可以设定每天最高可增加积分额度;

**新增功能**

- 微信小程序扫码登录网站，专属小程序在线扫码
- 基于位置信息的动态发布及文章推荐，可以让你快速找到附近的人。
- 丸子社区新版做了整体UI界面优化
- 圈子
- 新增了仿微信视频号功能
- 新增了发文章功能
- 新增了解析接口调整
- 新增了支持拼多多带货一键发布

### 小程序演示截图:

![wsVr1H.png](https://s1.ax1x.com/2020/09/14/wsVr1H.png)
![wsVfN8.png](https://s1.ax1x.com/2020/09/14/wsVfN8.png)
![wsVI3Q.png](https://s1.ax1x.com/2020/09/14/wsVI3Q.png)
一款UI设计精美的自媒体小程序目前支持四端两款模板，完全后台一键式配置，直接使用，基本不懂编程的小白都可以使用。

 **在线体验扫码NO1：** 

![wsmp01.jpg](https://s1.ax1x.com/2020/09/14/wsmp01.jpg)

 **在线体验扫码NO2：** 

![wsmn0I.jpg](https://s1.ax1x.com/2020/09/14/wsmn0I.jpg)

**基础功能**

- 增加自定义文章类型-公众号推文

- 支持创建焦点幻灯片图文显示

- 支持腾讯视频 / 微博视频解析

- 支持积分功能，签到打卡积分

- 支持多端积分付费阅读文章

- 支持 QQ 小程序消息模板通知

- 支持百度小程序消息模板通知

- 支持微信小程序关联公众号文章

- 支持网站后台下载小程序

小程序演示模板NO1:

![wsVTjs.png](https://s1.ax1x.com/2020/09/14/wsVTjs.png)
![wsVbBq.png](https://s1.ax1x.com/2020/09/14/wsVbBq.png)

小程序演示模板NO2:

![wsVz34.png](https://s1.ax1x.com/2020/09/14/wsVz34.png)
![wsZpv9.png](https://s1.ax1x.com/2020/09/14/wsZpv9.png)
![wsZPD1.png](https://s1.ax1x.com/2020/09/14/wsZPD1.png)
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。